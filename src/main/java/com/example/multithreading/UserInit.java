package com.example.multithreading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserInit implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    //多线程 hello 和 world交替输出，一秒一次

    @Autowired
    HelloService helloService;

    @Override
    public void run(String... args) throws Exception {
//        Thread thread = new Thread(new NewTask());
        Thread thread = new NewThread();
        thread.start();
        while (true){
            Thread.sleep(1000);
            String str = helloService.getWorld();
            logger.info(str);
        }

    }
}
