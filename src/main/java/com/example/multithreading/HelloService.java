package com.example.multithreading;

import org.springframework.stereotype.Service;

@Service
public class HelloService {


    public String getHello(){
        return "hello";
    }

    public String getWorld(){
        return "world";
    }

}
