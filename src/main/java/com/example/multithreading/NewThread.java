package com.example.multithreading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewThread extends Thread {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    HelloService helloService;

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String str = helloService.getHello();
            logger.info(""+str);
        }
    }
}
